(heading_1) @markup.heading
(heading_2) @markup.heading
(heading_3) @markup.heading

(block_quote) @markup.quote

(bullet_point_marker) @markup.list

(url) @markup.link.url
(link
  link_label: (text) @markup.link.text
)
