#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 43
#define LARGE_STATE_COUNT 4
#define SYMBOL_COUNT 27
#define ALIAS_COUNT 0
#define TOKEN_COUNT 12
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 1
#define MAX_ALIAS_SEQUENCE_LENGTH 5
#define PRODUCTION_ID_COUNT 3

enum {
  anon_sym_POUND = 1,
  anon_sym_POUND_POUND = 2,
  anon_sym_POUND_POUND_POUND = 3,
  sym_bullet_point_marker = 4,
  sym_link_marker = 5,
  anon_sym_ = 6,
  anon_sym_GT = 7,
  sym__word = 8,
  anon_sym_BQUOTE_BQUOTE_BQUOTE = 9,
  anon_sym_LF = 10,
  anon_sym_LF_BQUOTE_BQUOTE_BQUOTE = 11,
  sym_document = 12,
  sym_heading_1 = 13,
  sym_heading_2 = 14,
  sym_heading_3 = 15,
  sym_bullet_point = 16,
  sym_link = 17,
  sym_url = 18,
  sym_block_quote = 19,
  sym_text = 20,
  sym_preformatted_block = 21,
  sym_alt_text = 22,
  sym_line = 23,
  aux_sym_document_repeat1 = 24,
  aux_sym_text_repeat1 = 25,
  aux_sym_preformatted_block_repeat1 = 26,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_POUND] = "#",
  [anon_sym_POUND_POUND] = "##",
  [anon_sym_POUND_POUND_POUND] = "###",
  [sym_bullet_point_marker] = "bullet_point_marker",
  [sym_link_marker] = "link_marker",
  [anon_sym_] = " ",
  [anon_sym_GT] = ">",
  [sym__word] = "_word",
  [anon_sym_BQUOTE_BQUOTE_BQUOTE] = "```",
  [anon_sym_LF] = "\n",
  [anon_sym_LF_BQUOTE_BQUOTE_BQUOTE] = "\n```",
  [sym_document] = "document",
  [sym_heading_1] = "heading_1",
  [sym_heading_2] = "heading_2",
  [sym_heading_3] = "heading_3",
  [sym_bullet_point] = "bullet_point",
  [sym_link] = "link",
  [sym_url] = "url",
  [sym_block_quote] = "block_quote",
  [sym_text] = "text",
  [sym_preformatted_block] = "preformatted_block",
  [sym_alt_text] = "alt_text",
  [sym_line] = "line",
  [aux_sym_document_repeat1] = "document_repeat1",
  [aux_sym_text_repeat1] = "text_repeat1",
  [aux_sym_preformatted_block_repeat1] = "preformatted_block_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_POUND] = anon_sym_POUND,
  [anon_sym_POUND_POUND] = anon_sym_POUND_POUND,
  [anon_sym_POUND_POUND_POUND] = anon_sym_POUND_POUND_POUND,
  [sym_bullet_point_marker] = sym_bullet_point_marker,
  [sym_link_marker] = sym_link_marker,
  [anon_sym_] = anon_sym_,
  [anon_sym_GT] = anon_sym_GT,
  [sym__word] = sym__word,
  [anon_sym_BQUOTE_BQUOTE_BQUOTE] = anon_sym_BQUOTE_BQUOTE_BQUOTE,
  [anon_sym_LF] = anon_sym_LF,
  [anon_sym_LF_BQUOTE_BQUOTE_BQUOTE] = anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
  [sym_document] = sym_document,
  [sym_heading_1] = sym_heading_1,
  [sym_heading_2] = sym_heading_2,
  [sym_heading_3] = sym_heading_3,
  [sym_bullet_point] = sym_bullet_point,
  [sym_link] = sym_link,
  [sym_url] = sym_url,
  [sym_block_quote] = sym_block_quote,
  [sym_text] = sym_text,
  [sym_preformatted_block] = sym_preformatted_block,
  [sym_alt_text] = sym_alt_text,
  [sym_line] = sym_line,
  [aux_sym_document_repeat1] = aux_sym_document_repeat1,
  [aux_sym_text_repeat1] = aux_sym_text_repeat1,
  [aux_sym_preformatted_block_repeat1] = aux_sym_preformatted_block_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_POUND] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_POUND_POUND] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_POUND_POUND_POUND] = {
    .visible = true,
    .named = false,
  },
  [sym_bullet_point_marker] = {
    .visible = true,
    .named = true,
  },
  [sym_link_marker] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [sym__word] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_BQUOTE_BQUOTE_BQUOTE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LF] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LF_BQUOTE_BQUOTE_BQUOTE] = {
    .visible = true,
    .named = false,
  },
  [sym_document] = {
    .visible = true,
    .named = true,
  },
  [sym_heading_1] = {
    .visible = true,
    .named = true,
  },
  [sym_heading_2] = {
    .visible = true,
    .named = true,
  },
  [sym_heading_3] = {
    .visible = true,
    .named = true,
  },
  [sym_bullet_point] = {
    .visible = true,
    .named = true,
  },
  [sym_link] = {
    .visible = true,
    .named = true,
  },
  [sym_url] = {
    .visible = true,
    .named = true,
  },
  [sym_block_quote] = {
    .visible = true,
    .named = true,
  },
  [sym_text] = {
    .visible = true,
    .named = true,
  },
  [sym_preformatted_block] = {
    .visible = true,
    .named = true,
  },
  [sym_alt_text] = {
    .visible = true,
    .named = true,
  },
  [sym_line] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_document_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_text_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_preformatted_block_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_link_label = 1,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_link_label] = "link_label",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 1},
  [2] = {.index = 1, .length = 1},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_link_label, 3},
  [1] =
    {field_link_label, 4},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 15,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 14,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 28,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 41,
  [42] = 42,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(7);
      if (lookahead == '\n') ADVANCE(1);
      if (lookahead == '#') ADVANCE(8);
      if (lookahead == '*') ADVANCE(11);
      if (lookahead == '=') ADVANCE(16);
      if (lookahead == '>') ADVANCE(15);
      if (lookahead == '`') ADVANCE(18);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 1:
      if (lookahead == '\n') ADVANCE(1);
      if (lookahead == '#') ADVANCE(8);
      if (lookahead == '*') ADVANCE(11);
      if (lookahead == '=') ADVANCE(16);
      if (lookahead == '>') ADVANCE(15);
      if (lookahead == '`') ADVANCE(18);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(1)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 2:
      if (lookahead == '\n') ADVANCE(21);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(2)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 3:
      if (lookahead == '\n') ADVANCE(22);
      if (lookahead == ' ') ADVANCE(13);
      if (lookahead == '\t' ||
          lookahead == '\r') SKIP(3)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 4:
      if (lookahead == ' ') ADVANCE(14);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r') SKIP(4)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 5:
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(5)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 6:
      if (eof) ADVANCE(7);
      if (lookahead == '#') ADVANCE(8);
      if (lookahead == '*') ADVANCE(11);
      if (lookahead == '=') ADVANCE(16);
      if (lookahead == '>') ADVANCE(15);
      if (lookahead == '`') ADVANCE(18);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(6)
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 7:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 8:
      ACCEPT_TOKEN(anon_sym_POUND);
      if (lookahead == '#') ADVANCE(9);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 9:
      ACCEPT_TOKEN(anon_sym_POUND_POUND);
      if (lookahead == '#') ADVANCE(10);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 10:
      ACCEPT_TOKEN(anon_sym_POUND_POUND_POUND);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 11:
      ACCEPT_TOKEN(sym_bullet_point_marker);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 12:
      ACCEPT_TOKEN(sym_link_marker);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 13:
      ACCEPT_TOKEN(anon_sym_);
      if (lookahead == '\n') ADVANCE(22);
      if (lookahead == ' ') ADVANCE(13);
      END_STATE();
    case 14:
      ACCEPT_TOKEN(anon_sym_);
      if (lookahead == ' ') ADVANCE(14);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 16:
      ACCEPT_TOKEN(sym__word);
      if (lookahead == '>') ADVANCE(12);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 17:
      ACCEPT_TOKEN(sym__word);
      if (lookahead == '`') ADVANCE(20);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 18:
      ACCEPT_TOKEN(sym__word);
      if (lookahead == '`') ADVANCE(17);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 19:
      ACCEPT_TOKEN(sym__word);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 20:
      ACCEPT_TOKEN(anon_sym_BQUOTE_BQUOTE_BQUOTE);
      if (lookahead != 0 &&
          lookahead != '\t' &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != ' ') ADVANCE(19);
      END_STATE();
    case 21:
      ACCEPT_TOKEN(anon_sym_LF);
      if (lookahead == '\n') ADVANCE(21);
      END_STATE();
    case 22:
      ACCEPT_TOKEN(anon_sym_LF);
      if (lookahead == '\n') ADVANCE(22);
      if (lookahead == ' ') ADVANCE(13);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 6},
  [2] = {.lex_state = 6},
  [3] = {.lex_state = 6},
  [4] = {.lex_state = 6},
  [5] = {.lex_state = 6},
  [6] = {.lex_state = 6},
  [7] = {.lex_state = 6},
  [8] = {.lex_state = 2},
  [9] = {.lex_state = 2},
  [10] = {.lex_state = 2},
  [11] = {.lex_state = 3},
  [12] = {.lex_state = 2},
  [13] = {.lex_state = 2},
  [14] = {.lex_state = 2},
  [15] = {.lex_state = 2},
  [16] = {.lex_state = 5},
  [17] = {.lex_state = 5},
  [18] = {.lex_state = 5},
  [19] = {.lex_state = 2},
  [20] = {.lex_state = 5},
  [21] = {.lex_state = 4},
  [22] = {.lex_state = 5},
  [23] = {.lex_state = 5},
  [24] = {.lex_state = 2},
  [25] = {.lex_state = 5},
  [26] = {.lex_state = 5},
  [27] = {.lex_state = 5},
  [28] = {.lex_state = 3},
  [29] = {.lex_state = 3},
  [30] = {.lex_state = 3},
  [31] = {.lex_state = 2},
  [32] = {.lex_state = 2},
  [33] = {.lex_state = 0},
  [34] = {.lex_state = 2},
  [35] = {.lex_state = 2},
  [36] = {.lex_state = 2},
  [37] = {.lex_state = 2},
  [38] = {.lex_state = 2},
  [39] = {.lex_state = 2},
  [40] = {.lex_state = 2},
  [41] = {.lex_state = 2},
  [42] = {.lex_state = 2},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_POUND] = ACTIONS(1),
    [anon_sym_POUND_POUND] = ACTIONS(1),
    [anon_sym_POUND_POUND_POUND] = ACTIONS(1),
    [sym_bullet_point_marker] = ACTIONS(1),
    [sym_link_marker] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [sym__word] = ACTIONS(1),
    [anon_sym_BQUOTE_BQUOTE_BQUOTE] = ACTIONS(1),
    [anon_sym_LF_BQUOTE_BQUOTE_BQUOTE] = ACTIONS(1),
  },
  [1] = {
    [sym_document] = STATE(33),
    [sym_heading_1] = STATE(42),
    [sym_heading_2] = STATE(42),
    [sym_heading_3] = STATE(42),
    [sym_bullet_point] = STATE(42),
    [sym_link] = STATE(42),
    [sym_block_quote] = STATE(42),
    [sym_text] = STATE(42),
    [sym_preformatted_block] = STATE(3),
    [sym_line] = STATE(3),
    [aux_sym_document_repeat1] = STATE(3),
    [aux_sym_text_repeat1] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(3),
    [anon_sym_POUND] = ACTIONS(5),
    [anon_sym_POUND_POUND] = ACTIONS(7),
    [anon_sym_POUND_POUND_POUND] = ACTIONS(9),
    [sym_bullet_point_marker] = ACTIONS(11),
    [sym_link_marker] = ACTIONS(13),
    [anon_sym_GT] = ACTIONS(15),
    [sym__word] = ACTIONS(17),
    [anon_sym_BQUOTE_BQUOTE_BQUOTE] = ACTIONS(19),
  },
  [2] = {
    [sym_heading_1] = STATE(42),
    [sym_heading_2] = STATE(42),
    [sym_heading_3] = STATE(42),
    [sym_bullet_point] = STATE(42),
    [sym_link] = STATE(42),
    [sym_block_quote] = STATE(42),
    [sym_text] = STATE(42),
    [sym_preformatted_block] = STATE(2),
    [sym_line] = STATE(2),
    [aux_sym_document_repeat1] = STATE(2),
    [aux_sym_text_repeat1] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(21),
    [anon_sym_POUND] = ACTIONS(23),
    [anon_sym_POUND_POUND] = ACTIONS(26),
    [anon_sym_POUND_POUND_POUND] = ACTIONS(29),
    [sym_bullet_point_marker] = ACTIONS(32),
    [sym_link_marker] = ACTIONS(35),
    [anon_sym_GT] = ACTIONS(38),
    [sym__word] = ACTIONS(41),
    [anon_sym_BQUOTE_BQUOTE_BQUOTE] = ACTIONS(44),
  },
  [3] = {
    [sym_heading_1] = STATE(42),
    [sym_heading_2] = STATE(42),
    [sym_heading_3] = STATE(42),
    [sym_bullet_point] = STATE(42),
    [sym_link] = STATE(42),
    [sym_block_quote] = STATE(42),
    [sym_text] = STATE(42),
    [sym_preformatted_block] = STATE(2),
    [sym_line] = STATE(2),
    [aux_sym_document_repeat1] = STATE(2),
    [aux_sym_text_repeat1] = STATE(19),
    [ts_builtin_sym_end] = ACTIONS(47),
    [anon_sym_POUND] = ACTIONS(5),
    [anon_sym_POUND_POUND] = ACTIONS(7),
    [anon_sym_POUND_POUND_POUND] = ACTIONS(9),
    [sym_bullet_point_marker] = ACTIONS(11),
    [sym_link_marker] = ACTIONS(13),
    [anon_sym_GT] = ACTIONS(15),
    [sym__word] = ACTIONS(17),
    [anon_sym_BQUOTE_BQUOTE_BQUOTE] = ACTIONS(19),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 2,
    ACTIONS(49), 1,
      ts_builtin_sym_end,
    ACTIONS(51), 8,
      anon_sym_POUND,
      anon_sym_POUND_POUND,
      anon_sym_POUND_POUND_POUND,
      sym_bullet_point_marker,
      sym_link_marker,
      anon_sym_GT,
      sym__word,
      anon_sym_BQUOTE_BQUOTE_BQUOTE,
  [14] = 2,
    ACTIONS(53), 1,
      ts_builtin_sym_end,
    ACTIONS(55), 8,
      anon_sym_POUND,
      anon_sym_POUND_POUND,
      anon_sym_POUND_POUND_POUND,
      sym_bullet_point_marker,
      sym_link_marker,
      anon_sym_GT,
      sym__word,
      anon_sym_BQUOTE_BQUOTE_BQUOTE,
  [28] = 2,
    ACTIONS(57), 1,
      ts_builtin_sym_end,
    ACTIONS(59), 8,
      anon_sym_POUND,
      anon_sym_POUND_POUND,
      anon_sym_POUND_POUND_POUND,
      sym_bullet_point_marker,
      sym_link_marker,
      anon_sym_GT,
      sym__word,
      anon_sym_BQUOTE_BQUOTE_BQUOTE,
  [42] = 2,
    ACTIONS(61), 1,
      ts_builtin_sym_end,
    ACTIONS(63), 8,
      anon_sym_POUND,
      anon_sym_POUND_POUND,
      anon_sym_POUND_POUND_POUND,
      sym_bullet_point_marker,
      sym_link_marker,
      anon_sym_GT,
      sym__word,
      anon_sym_BQUOTE_BQUOTE_BQUOTE,
  [56] = 5,
    ACTIONS(65), 1,
      sym__word,
    ACTIONS(67), 1,
      anon_sym_LF,
    ACTIONS(69), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    STATE(15), 1,
      aux_sym_text_repeat1,
    STATE(12), 2,
      sym_text,
      aux_sym_preformatted_block_repeat1,
  [73] = 5,
    ACTIONS(65), 1,
      sym__word,
    ACTIONS(71), 1,
      anon_sym_LF,
    ACTIONS(73), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    STATE(15), 1,
      aux_sym_text_repeat1,
    STATE(10), 2,
      sym_text,
      aux_sym_preformatted_block_repeat1,
  [90] = 5,
    ACTIONS(65), 1,
      sym__word,
    ACTIONS(69), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    ACTIONS(75), 1,
      anon_sym_LF,
    STATE(15), 1,
      aux_sym_text_repeat1,
    STATE(13), 2,
      sym_text,
      aux_sym_preformatted_block_repeat1,
  [107] = 6,
    ACTIONS(17), 1,
      sym__word,
    ACTIONS(77), 1,
      anon_sym_,
    ACTIONS(79), 1,
      anon_sym_LF,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(34), 1,
      sym_alt_text,
    STATE(36), 1,
      sym_text,
  [126] = 5,
    ACTIONS(65), 1,
      sym__word,
    ACTIONS(75), 1,
      anon_sym_LF,
    ACTIONS(81), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    STATE(15), 1,
      aux_sym_text_repeat1,
    STATE(13), 2,
      sym_text,
      aux_sym_preformatted_block_repeat1,
  [143] = 5,
    ACTIONS(83), 1,
      sym__word,
    ACTIONS(86), 1,
      anon_sym_LF,
    ACTIONS(89), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    STATE(15), 1,
      aux_sym_text_repeat1,
    STATE(13), 2,
      sym_text,
      aux_sym_preformatted_block_repeat1,
  [160] = 4,
    ACTIONS(91), 1,
      sym__word,
    ACTIONS(94), 1,
      anon_sym_LF,
    ACTIONS(96), 1,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
    STATE(14), 1,
      aux_sym_text_repeat1,
  [173] = 3,
    ACTIONS(100), 1,
      anon_sym_LF,
    STATE(14), 1,
      aux_sym_text_repeat1,
    ACTIONS(98), 2,
      sym__word,
      anon_sym_LF_BQUOTE_BQUOTE_BQUOTE,
  [184] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(31), 1,
      sym_text,
  [194] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(41), 1,
      sym_text,
  [204] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(32), 1,
      sym_text,
  [214] = 3,
    ACTIONS(100), 1,
      anon_sym_LF,
    ACTIONS(104), 1,
      sym__word,
    STATE(24), 1,
      aux_sym_text_repeat1,
  [224] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(39), 1,
      sym_text,
  [234] = 3,
    ACTIONS(106), 1,
      anon_sym_,
    ACTIONS(108), 1,
      sym__word,
    STATE(29), 1,
      sym_url,
  [244] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(37), 1,
      sym_text,
  [254] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(38), 1,
      sym_text,
  [264] = 3,
    ACTIONS(94), 1,
      anon_sym_LF,
    ACTIONS(110), 1,
      sym__word,
    STATE(24), 1,
      aux_sym_text_repeat1,
  [274] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(35), 1,
      sym_text,
  [284] = 3,
    ACTIONS(102), 1,
      sym__word,
    STATE(19), 1,
      aux_sym_text_repeat1,
    STATE(40), 1,
      sym_text,
  [294] = 2,
    ACTIONS(113), 1,
      sym__word,
    STATE(30), 1,
      sym_url,
  [301] = 1,
    ACTIONS(115), 2,
      anon_sym_,
      anon_sym_LF,
  [306] = 2,
    ACTIONS(117), 1,
      anon_sym_,
    ACTIONS(119), 1,
      anon_sym_LF,
  [313] = 2,
    ACTIONS(121), 1,
      anon_sym_,
    ACTIONS(123), 1,
      anon_sym_LF,
  [320] = 1,
    ACTIONS(125), 1,
      anon_sym_LF,
  [324] = 1,
    ACTIONS(127), 1,
      anon_sym_LF,
  [328] = 1,
    ACTIONS(129), 1,
      ts_builtin_sym_end,
  [332] = 1,
    ACTIONS(131), 1,
      anon_sym_LF,
  [336] = 1,
    ACTIONS(133), 1,
      anon_sym_LF,
  [340] = 1,
    ACTIONS(135), 1,
      anon_sym_LF,
  [344] = 1,
    ACTIONS(137), 1,
      anon_sym_LF,
  [348] = 1,
    ACTIONS(139), 1,
      anon_sym_LF,
  [352] = 1,
    ACTIONS(141), 1,
      anon_sym_LF,
  [356] = 1,
    ACTIONS(143), 1,
      anon_sym_LF,
  [360] = 1,
    ACTIONS(145), 1,
      anon_sym_LF,
  [364] = 1,
    ACTIONS(147), 1,
      anon_sym_LF,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(4)] = 0,
  [SMALL_STATE(5)] = 14,
  [SMALL_STATE(6)] = 28,
  [SMALL_STATE(7)] = 42,
  [SMALL_STATE(8)] = 56,
  [SMALL_STATE(9)] = 73,
  [SMALL_STATE(10)] = 90,
  [SMALL_STATE(11)] = 107,
  [SMALL_STATE(12)] = 126,
  [SMALL_STATE(13)] = 143,
  [SMALL_STATE(14)] = 160,
  [SMALL_STATE(15)] = 173,
  [SMALL_STATE(16)] = 184,
  [SMALL_STATE(17)] = 194,
  [SMALL_STATE(18)] = 204,
  [SMALL_STATE(19)] = 214,
  [SMALL_STATE(20)] = 224,
  [SMALL_STATE(21)] = 234,
  [SMALL_STATE(22)] = 244,
  [SMALL_STATE(23)] = 254,
  [SMALL_STATE(24)] = 264,
  [SMALL_STATE(25)] = 274,
  [SMALL_STATE(26)] = 284,
  [SMALL_STATE(27)] = 294,
  [SMALL_STATE(28)] = 301,
  [SMALL_STATE(29)] = 306,
  [SMALL_STATE(30)] = 313,
  [SMALL_STATE(31)] = 320,
  [SMALL_STATE(32)] = 324,
  [SMALL_STATE(33)] = 328,
  [SMALL_STATE(34)] = 332,
  [SMALL_STATE(35)] = 336,
  [SMALL_STATE(36)] = 340,
  [SMALL_STATE(37)] = 344,
  [SMALL_STATE(38)] = 348,
  [SMALL_STATE(39)] = 352,
  [SMALL_STATE(40)] = 356,
  [SMALL_STATE(41)] = 360,
  [SMALL_STATE(42)] = 364,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_document, 0),
  [5] = {.entry = {.count = 1, .reusable = false}}, SHIFT(17),
  [7] = {.entry = {.count = 1, .reusable = false}}, SHIFT(26),
  [9] = {.entry = {.count = 1, .reusable = false}}, SHIFT(23),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(22),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(21),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(16),
  [17] = {.entry = {.count = 1, .reusable = false}}, SHIFT(19),
  [19] = {.entry = {.count = 1, .reusable = false}}, SHIFT(11),
  [21] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_document_repeat1, 2),
  [23] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(17),
  [26] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(26),
  [29] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(23),
  [32] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(22),
  [35] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(21),
  [38] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(16),
  [41] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(19),
  [44] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_document_repeat1, 2), SHIFT_REPEAT(11),
  [47] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_document, 1),
  [49] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_preformatted_block, 4),
  [51] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_preformatted_block, 4),
  [53] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_preformatted_block, 3),
  [55] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_preformatted_block, 3),
  [57] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_line, 2),
  [59] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_line, 2),
  [61] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_preformatted_block, 5),
  [63] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_preformatted_block, 5),
  [65] = {.entry = {.count = 1, .reusable = false}}, SHIFT(15),
  [67] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [69] = {.entry = {.count = 1, .reusable = false}}, SHIFT(4),
  [71] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [73] = {.entry = {.count = 1, .reusable = false}}, SHIFT(5),
  [75] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [77] = {.entry = {.count = 1, .reusable = false}}, SHIFT(18),
  [79] = {.entry = {.count = 1, .reusable = false}}, SHIFT(9),
  [81] = {.entry = {.count = 1, .reusable = false}}, SHIFT(7),
  [83] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_preformatted_block_repeat1, 2), SHIFT_REPEAT(15),
  [86] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_preformatted_block_repeat1, 2), SHIFT_REPEAT(13),
  [89] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_preformatted_block_repeat1, 2),
  [91] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_text_repeat1, 2), SHIFT_REPEAT(14),
  [94] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_text_repeat1, 2),
  [96] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_text_repeat1, 2),
  [98] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_text, 1),
  [100] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_text, 1),
  [102] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [104] = {.entry = {.count = 1, .reusable = false}}, SHIFT(24),
  [106] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [108] = {.entry = {.count = 1, .reusable = false}}, SHIFT(28),
  [110] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_text_repeat1, 2), SHIFT_REPEAT(24),
  [113] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [115] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_url, 1),
  [117] = {.entry = {.count = 1, .reusable = false}}, SHIFT(25),
  [119] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_link, 2),
  [121] = {.entry = {.count = 1, .reusable = false}}, SHIFT(20),
  [123] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_link, 3),
  [125] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block_quote, 2),
  [127] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_alt_text, 2),
  [129] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [131] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [133] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_link, 4, .production_id = 1),
  [135] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_alt_text, 1),
  [137] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_bullet_point, 2),
  [139] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_heading_3, 2),
  [141] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_link, 5, .production_id = 2),
  [143] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_heading_2, 2),
  [145] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_heading_1, 2),
  [147] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_gemtext(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
