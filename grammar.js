module.exports = grammar({
  name: 'gemtext',

  rules: {
    document: ($) => repeat(choice($.line, $.preformatted_block)),

    heading_1: ($) => seq('#', $.text),
    heading_2: ($) => seq('##', $.text),
    heading_3: ($) => seq('###', $.text),

    bullet_point: ($) => seq($.bullet_point_marker, $.text),
    bullet_point_marker: ($) => '*',

    // TODO: fix links with no space after arrow
    // like `=>gemini://blube.club`
    link_marker: ($) => '=>',
    link: ($) =>
      seq(
        $.link_marker,
        optional(' '),
        $.url,
        optional(seq(' ', field('link_label', $.text))),
      ),

    url: ($) => $._word,

    block_quote: ($) => seq('>', $.text),

    _word: ($) => /[^\n\r\t ]+/,

    text: ($) => prec.right(1, repeat1($._word)),

    preformatted_block: ($) =>
      seq(
        '```',
        optional($.alt_text),
        '\n',
        repeat(choice($.text, '\n')),
        '\n```',
      ),

    alt_text: ($) => seq(optional(' '), $.text),

    line: ($) =>
      seq(
        choice(
          $.block_quote,
          $.bullet_point,
          $.heading_1,
          $.heading_2,
          $.heading_3,
          $.link,
          $.text,
        ),
        '\n',
      ),
  },
})
