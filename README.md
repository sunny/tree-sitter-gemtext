# tree-sitter-gemtext

a WIP [tree-sitter grammar](https://tree-sitter.github.io) for Gemtext (.gmi) files

## running

install dependencies:

```
npm i
```

you also need the [tree-sitter-cli](https://tree-sitter.github.io/tree-sitter/creating-parsers#installation). then you can generate the parser from the grammar:
```
tree-sitter generate
```

and you can see the syntax tree of the example with:

```
tree-sitter parse example
```
