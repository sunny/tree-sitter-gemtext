==================
heading with paragraph and bullet points
==================

# heya this is a heading

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat.

hey




* a-one
* a-two
* a-one-two
* three-four

---

(document
  (line
    (heading_1
      (text)))
  (line
    (text))
  (line
    (text))
  (line
    (bullet_point
      (bullet_point_marker)
      (text)))
  (line
    (bullet_point
      (bullet_point_marker)
      (text)))
  (line
    (bullet_point
      (bullet_point_marker)
      (text)))
  (line
    (bullet_point
      (bullet_point_marker)
      (text)))
)
